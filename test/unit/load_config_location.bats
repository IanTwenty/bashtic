#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# A BASH wrapper for restic
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

bats_require_minimum_version 1.5.0

source "$BATS_TEST_DIRNAME"/../../bashtic
load ../test_helper/bats-support/load
load ../test_helper/bats-assert/load

################################################################################
#
# Tests
#
################################################################################

@test "to must be specified" {
  config=""

  run ! load_config_location <(echo "$config")

  assert_output --regexp "Location .*: 'to' is missing but it's not optional!"
}

@test "from must be array" {
  config="to=('backend');from='thing'"

  run ! load_config_location <(echo "$config")

  assert_output --regexp "Location .*: 'from' must be an array."
}

@test "forget must be assoc. array with right keys" {
  config="to=('backend');from=('/home');forget='thing'"
  run ! load_config_location <(echo "$config")
  assert_output --regexp "Location .*: 'forget' must be an associative array."

  # It would be nice to test that user hasn't set forget to an indexed array but
  # there is no way to know tell the difference between the two in bash

  config="to=('backend');from=('/home');forget=(['thing']=9)"
  run ! load_config_location <(echo "$config")
  assert_output --regexp "Location .*: 'forget' had an unrecognised entry: thing"
}

@test "forget with all the right keys is ok" {
  config="to=('backend');from=('/home');\
  forget=(\
    ['keep-last']='99'\
    ['keep-hourly']='99'\
    ['keep-daily']='99'\
    ['keep-weekly']='99'\
    ['keep-monthly']='99'\
    ['keep-yearly']='99'\
  )"
  load_config_location <(echo "$config")
  # Also check forget still exists in this scope as we redeclare it inside load_config_locate
  assert_equal "$(declare -p forget)" 'declare -A forget=([keep-yearly]="99" [keep-monthly]="99" [keep-daily]="99" [keep-last]="99" [keep-weekly]="99" [keep-hourly]="99" )'
}

@test "forget_flags must be array" {
  config="to=('backend');forget=(['keep-last']=9);forget_flags='thing'"

  run ! load_config_location <(echo "$config")

  assert_output --regexp "Location .*: 'forget_flags' must be an array."
}

@test "cludes must be array" {
  config="to=('backend');cludes='thing'"

  run ! load_config_location <(echo "$config")

  assert_output --regexp "Location .*: 'cludes' must be an array."
}

@test "valid config sets runstate var" {
  local _bashtic_runstate_var="_bashtic_$$_RUNSTATE"
  config="to=('backend');from=('/home');forget_flags=('--prune');\
  forget=(\
    ['keep-last']='1'\
    ['keep-hourly']='2'\
    ['keep-daily']='3'\
    ['keep-weekly']='4'\
    ['keep-monthly']='5'\
    ['keep-yearly']='6'\
  )\
  cludes=(\
    '/home#i'\
  )"

  load_config_location <(echo "$config")

  assert_equal "${!_bashtic_runstate_var}" "\
unset from;declare -g -a from=([0]=\"/home\");\
unset to;declare -g -a to=([0]=\"backend\");\
unset forget;declare -g -A forget=([keep-yearly]=\"6\" [keep-monthly]=\"5\" [keep-daily]=\"3\" [keep-last]=\"1\" [keep-weekly]=\"4\" [keep-hourly]=\"2\" );\
unset forget_flags;declare -g -a forget_flags=([0]=\"--prune\");\
unset cludes;declare -g -a cludes=([0]=\"/home#i\")"
}

@test "extra config ignored" {
  local _bashtic_runstate_var="_bashtic_$$_RUNSTATE"
  blah=safe
  config="to=('backend');from=('/home');forget_flags=('--prune');\
  forget=(\
    ['keep-last']='1'\
    ['keep-hourly']='2'\
    ['keep-daily']='3'\
    ['keep-weekly']='4'\
    ['keep-monthly']='5'\
    ['keep-yearly']='6'\
  )\
  cludes=(\
    '/home#i'\
  );\
  blah=changed"

  load_config_location <(echo "$config")

  assert_equal "$blah" "safe"
}
