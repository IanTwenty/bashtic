# SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# A BASH wrapper for restic
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

include build_config.ninja
include galagos/build.ninja

build build: phony
build test: phony $
  galagos_compliance $
  lint $
  bats $
  docs
default test
build update: phony $
  galagos_update
build deploy: phony

# Ninjasub - https://gitlab.com/IanTwenty/ninjasub#one-off-configuration-of-your-build-file

rule ninjasub
  command = /home/ian/Documents/Code/gitlab.com/ninjasub/ninjasub ${builddir}/sentinal.ninjasub

build ${builddir}/sentinal.ninjasub: phony
build build.ninja: ninjasub ${builddir}/sentinal.ninjasub

# Linting

rule mdl
  command = mdl -g . && touch ${out} && echo "Yes"
rule shellcheck
  command = shellcheck ${in}

build lint: phony ${builddir}/sentinal.mdl ${builddir}/sentinal.shellcheck
# ninjasub:*.md
build ${builddir}/sentinal.mdl: mdl $
                                ./AUTHORS.md $
                                ./CHANGELOG.md $
                                ./CODE_OF_CONDUCT.md $
                                ./CONTRIBUTING.md $
                                ./docs/CLI/index.md $
                                ./docs/Cludes.md $
                                ./docs/Configuration/Backends.md $
                                ./docs/Configuration/index.md $
                                ./docs/Configuration/Locations.md $
                                ./docs/Configuration/Overview.md $
                                ./docs/Configuration/Pipelines.md $
                                ./docs/Examples.md $
                                ./docs/index.md $
                                ./docs/Installation.md $
                                ./docs/Introduction.md $
                                ./docs/QuickStart.md $
                                ./docs/Roadmap.md $
                                ./README.md $
                                ./test/test_helper/bats-assert/README.md $
                                ./test/test_helper/bats-mock/README.md $
                                ./test/test_helper/bats-support/CHANGELOG.md $
                                ./test/test_helper/bats-support/README.md

build ${builddir}/sentinal.shellcheck: shellcheck $
                                       bashtic

# Bats Tests

rule bats
  command = bats test/unit test/integration && touch ${out}
  pool = console

build bats: phony ${builddir}/sentinal.bats
# ninjasub:./test/*
build ${builddir}/sentinal.bats: bats $
                                 ./test/integration $
                                 ./test/integration/integration.bats $
                                 ./test/test_helper $
                                 ./test/test_helper/bats-assert $
                                 ./test/test_helper/bats-assert/.git $
                                 ./test/test_helper/bats-assert/.github $
                                 ./test/test_helper/bats-assert/.github/workflows $
                                 ./test/test_helper/bats-assert/.github/workflows/tests.yml $
                                 ./test/test_helper/bats-assert/.gitignore $
                                 ./test/test_helper/bats-assert/LICENSE $
                                 ./test/test_helper/bats-assert/load.bash $
                                 ./test/test_helper/bats-assert/package.json $
                                 ./test/test_helper/bats-assert/README.md $
                                 ./test/test_helper/bats-assert/src $
                                 ./test/test_helper/bats-assert/src/assert.bash $
                                 ./test/test_helper/bats-assert/src/assert_equal.bash $
                                 ./test/test_helper/bats-assert/src/assert_failure.bash $
                                 ./test/test_helper/bats-assert/src/assert_line.bash $
                                 ./test/test_helper/bats-assert/src/assert_not_equal.bash $
                                 ./test/test_helper/bats-assert/src/assert_output.bash $
                                 ./test/test_helper/bats-assert/src/assert_regex.bash $
                                 ./test/test_helper/bats-assert/src/assert_success.bash $
                                 ./test/test_helper/bats-assert/src/refute.bash $
                                 ./test/test_helper/bats-assert/src/refute_line.bash $
                                 ./test/test_helper/bats-assert/src/refute_output.bash $
                                 ./test/test_helper/bats-assert/src/refute_regex.bash $
                                 ./test/test_helper/bats-assert/test $
                                 ./test/test_helper/bats-assert/test/assert.bats $
                                 ./test/test_helper/bats-assert/test/assert_equal.bats $
                                 ./test/test_helper/bats-assert/test/assert_failure.bats $
                                 ./test/test_helper/bats-assert/test/assert_line.bats $
                                 ./test/test_helper/bats-assert/test/assert_not_equal.bats $
                                 ./test/test_helper/bats-assert/test/assert_output.bats $
                                 ./test/test_helper/bats-assert/test/assert_regex.bats $
                                 ./test/test_helper/bats-assert/test/assert_success.bats $
                                 ./test/test_helper/bats-assert/test/refute.bats $
                                 ./test/test_helper/bats-assert/test/refute_line.bats $
                                 ./test/test_helper/bats-assert/test/refute_output.bats $
                                 ./test/test_helper/bats-assert/test/refute_regex.bats $
                                 ./test/test_helper/bats-assert/test/test_helper.bash $
                                 ./test/test_helper/bats-mock $
                                 ./test/test_helper/bats-mock/build $
                                 ./test/test_helper/bats-mock/.git $
                                 ./test/test_helper/bats-mock/.gitignore $
                                 ./test/test_helper/bats-mock/LICENSE $
                                 ./test/test_helper/bats-mock/load.bash $
                                 ./test/test_helper/bats-mock/README.md $
                                 ./test/test_helper/bats-mock/script $
                                 ./test/test_helper/bats-mock/script/install_bats $
                                 ./test/test_helper/bats-mock/src $
                                 ./test/test_helper/bats-mock/src/bats-mock.bash $
                                 ./test/test_helper/bats-mock/test $
                                 ./test/test_helper/bats-mock/test/mock_create.bats $
                                 ./test/test_helper/bats-mock/test/mock_get_call_args.bats $
                                 ./test/test_helper/bats-mock/test/mock_get_call_env.bats $
                                 ./test/test_helper/bats-mock/test/mock_get_call_num.bats $
                                 ./test/test_helper/bats-mock/test/mock_get_call_user.bats $
                                 ./test/test_helper/bats-mock/test/mock_set_output.bats $
                                 ./test/test_helper/bats-mock/test/mock_set_side_effect.bats $
                                 ./test/test_helper/bats-mock/test/mock_set_status.bats $
                                 ./test/test_helper/bats-mock/test/mock_test_suite.bash $
                                 ./test/test_helper/bats-mock/.travis.yml $
                                 ./test/test_helper/bats-support $
                                 ./test/test_helper/bats-support/CHANGELOG.md $
                                 ./test/test_helper/bats-support/.git $
                                 ./test/test_helper/bats-support/.github $
                                 ./test/test_helper/bats-support/.github/workflows $
                                 ./test/test_helper/bats-support/.github/workflows/tests.yml $
                                 ./test/test_helper/bats-support/.gitignore $
                                 ./test/test_helper/bats-support/LICENSE $
                                 ./test/test_helper/bats-support/load.bash $
                                 ./test/test_helper/bats-support/package.json $
                                 ./test/test_helper/bats-support/README.md $
                                 ./test/test_helper/bats-support/src $
                                 ./test/test_helper/bats-support/src/error.bash $
                                 ./test/test_helper/bats-support/src/lang.bash $
                                 ./test/test_helper/bats-support/src/output.bash $
                                 ./test/test_helper/bats-support/test $
                                 ./test/test_helper/bats-support/test/50-output-10-batslib_err.bats $
                                 ./test/test_helper/bats-support/test/50-output-11-batslib_count_lines.bats $
                                 ./test/test_helper/bats-support/test/50-output-12-batslib_is_single_line.bats $
                                 ./test/test_helper/bats-support/test/50-output-13-batslib_get_max_single_line_key_width.bats $
                                 ./test/test_helper/bats-support/test/50-output-14-batslib_print_kv_single.bats $
                                 ./test/test_helper/bats-support/test/50-output-15-batslib_print_kv_multi.bats $
                                 ./test/test_helper/bats-support/test/50-output-16-batslib_print_kv_single_or_multi.bats $
                                 ./test/test_helper/bats-support/test/50-output-17-batslib_prefix.bats $
                                 ./test/test_helper/bats-support/test/50-output-18-batslib_mark.bats $
                                 ./test/test_helper/bats-support/test/50-output-19-batslib_decorate.bats $
                                 ./test/test_helper/bats-support/test/51-error-10-fail.bats $
                                 ./test/test_helper/bats-support/test/52-lang-10-batslib_is_caller.bats $
                                 ./test/test_helper/bats-support/test/test_helper.bash $
                                 ./test/unit $
                                 ./test/unit/backup.bats $
                                 ./test/unit/check.bats $
                                 ./test/unit/excludes_file.bats $
                                 ./test/unit/forget.bats $
                                 ./test/unit/load_config_backend.bats $
                                 ./test/unit/load_config.bats $
                                 ./test/unit/load_config_location.bats $
                                 ./test/unit/pipeline_functions.bats $
                                 ./test/unit/pipeline_run.bats $
                                 ./test/unit/restic_env.bats $
                                 ./test/unit/restore.bats $
                                 ./test/unit/save_runstate.bats
rule venv
  command = python -m venv ${builddir}/venv && $
            . ${builddir}/venv/bin/activate && $
            pip install --upgrade pip && $
            pip install -r docs/requirements.txt
  pool = console

rule sphinx-build
  command = . ${builddir}/venv/bin/activate && $
            sphinx-build -E -a -b html docs ${builddir}/docs
  pool = console

build docs: phony sphinx-build
build venv: phony ${builddir}/venv
build ${builddir}/venv: venv
build sphinx-build: phony ${builddir}/docs/index.html
# ninjasub:./docs/*
build ${builddir}/docs/index.html: sphinx-build venv $
                                   ./docs/CLI $
                                   ./docs/CLI/index.md $
                                   ./docs/Cludes.md $
                                   ./docs/Configuration $
                                   ./docs/Configuration/Backends.md $
                                   ./docs/Configuration/index.md $
                                   ./docs/Configuration/Locations.md $
                                   ./docs/Configuration/Overview.md $
                                   ./docs/Configuration/Pipelines.md $
                                   ./docs/conf.py $
                                   ./docs/Examples.md $
                                   ./docs/index.md $
                                   ./docs/Installation.md $
                                   ./docs/Introduction.md $
                                   ./docs/QuickStart.md $
                                   ./docs/requirements.txt $
                                   ./docs/Roadmap.md
# Helper Targets

build _addheaders: phony $
                   _addheaders_cc-by-sa_html $
                   _addheaders_cc-by-sa_html_explicit $
                   _addheaders_cc-by-sa_python $
                   _addheaders_gpl3_python

build _addheaders_cc-by-sa_html: galagos_addheader $
                                 AUTHORS.md $
                                 CHANGELOG.md $
                                 CONTRIBUTING.md $
                                 DESIGN.md $
                                 README.md $
                                 docs/CLI/index.md $
                                 docs/Configuration/Backends.md $
                                 docs/Configuration/Locations.md $
                                 docs/Configuration/Overview.md $
                                 docs/Configuration/Pipelines.md $
                                 docs/Cludes.md $
                                 docs/Examples.md $
                                 docs/Installation.md $
                                 docs/Introduction.md $
                                 docs/QuickStart.md $
                                 docs/index.md
  galagos_license=CC-BY-SA-4.0
  galagos_style=html

build _addheaders_cc-by-sa_html_explicit: galagos_addheader $
                                         .copier-answers.yml
  galagos_license=CC-BY-SA-4.0
  galagos_style=html
  galagos_reuse_explicit_switch=--explicit-license

build _addheaders_cc-by-sa_python: galagos_addheader $
                                    .gitignore $
                                    .gitmodules $
                                    .readthedocs.yaml $
                                    docs/requirements.txt
  galagos_license=CC-BY-SA-4.0
  galagos_style=python

build _addheaders_gpl3_python: galagos_addheader $
                               bashtic $
                               build.ninja $
                               docs/conf.py $
                               galagos/build.ninja $
                               test/unit/excludes_file.bats $
                               test/unit/backup.bats $
                               test/unit/check.bats $
                               test/unit/forget.bats $
                               test/unit/load_config.bats $
                               test/unit/load_config_backend.bats $
                               test/unit/load_config_location.bats $
                               test/unit/restic_env.bats $
                               test/unit/pipeline_functions.bats $
                               test/unit/pipeline_run.bats
  galagos_license=GPL-3.0-or-later
  galagos_style=python
