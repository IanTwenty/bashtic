<!--
SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

A BASH wrapper for restic

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Backends

Backends define a restic repository target. They must define:

* Their `type`
* Their `path`

At the moment we only support one `type` but more are on the roadmap.

Optionally they can also define some restic environment variables to handle
credentials:

* `RESTIC_PASSWORD`
* `RESTIC_PASSWORD_COMMAND`
* `RESTIC_PASSWORD_FILE`

Backend configs are just files under the dir:

```bash
$XDG_CONFIG_DIR/bashtic/backends
```

## `type`

This variable can only take one value for now:

```bash
type="local"
```

Which indicates a repository on the local system.

## `path`

This variable specifies the location of the repository as a path on the local
system:

```bash
path="/opt/backups/restic"
```

Of course it can be external storage or anything reachable in your local
filesystem:

```bash
path="/run/media/$USER/Backup/restic"
```

## `RESTIC_PASSWORD`

This variable can be used to supply the restic repository password and avoid
being prompted. This is probably not advisable as you're potentially leaving
your password available to other users and/or processes unencrypted.

```bash
RESTIC_PASSWORD=dontdothis
```

## `RESTIC_PASSWORD_COMMAND`

This variable is used to supply a command for restic to run that will supply the
password on stdout.

```bash
RESTIC_PASSWORD_COMMAND=secret-tool lookup key
```

## `RESTIC_PASSWORD_FILE`

This variable is used to supply the location of a password file. Again you
should be careful with the permissions of this file and consider whether you're
leaving your password available unencrypted to other users and/or process.

```bash
RESTIC_PASSWORD_FILE=/home/you/.secrets/passwordfile
```
