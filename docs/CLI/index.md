<!--
SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

A BASH wrapper for restic

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# CLI

Bashtic is simple to invoke:

```bash
bashtic [options] [<pipeline>[<@backend>]] -- [pipeline args] ...
```

Specifying a `pipeline` is optional. A pipeline called `default` is assumed if
one is not provided. Bashtic also comes with several [built-in
pipelines](../Configuration/Pipelines.md#built-in-pipelines).

You can optionally specify a backend with the `@backend` syntax otherwise
bashtic will run the pipeline against all backends.

The possible `options` are:

* `-n` or `--dryrun`: Dryrun mode - print restic commands rather than invoke.
* `-s` or `--snapshot`: For restore operations: specify the snapshot ID to
  restore (or `latest`).
* `-t` or `--to`: For restore operations: specify the dir to restore to.

You can pass arguments to your pipeline by specifying them after the separator
`--`, e.g.

```bash
bashtic mypipeline -- arg1 arg2
```
