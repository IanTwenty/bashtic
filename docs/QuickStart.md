<!--
SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

A BASH wrapper for restic

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Quick Start

This guide will get you up and running. It was last tested on the 1st March 2023
with debian stable (restic v0.11.0, BASH v5.1.4)

In this guide we'll do the following:

* Define a location (a backup source)
* Define a backend (a restic repository)
* Run bashtic
* Create a custom pipeline
* Tidy up
* Next moves

We'll create a temporary restic repository so there's no risk of harm to your
existing repositories.

Prerequisites:

* If you've not installed bashtic refer to our [installation](Installation) instructions.
* The example code is all BASH, so we assume you are in a BASH shell.
* Restic should also be installed of course.

## Define a location

If we run bashtic straight after installation we'll get an error:

```bash
$ bashtic
No location found at /home/you/.config/bashtic/locations/default
```

Bashtic requires configuration before it can do anything meaningful. The first
step is to define a location which will be backed up. Locations described what
is to be backed up and where it should be backed up to.

Let's backup our home dir and make it the default location. We configure
bashtic's default location by creating a file
`$HOME/.config/bashtic/locations/default`:

```bash
mkdir -p ~/.config/bashtic/locations
cat > ~/.config/bashtic/locations/default << EOF
from=("$HOME")
to=("localdir")
EOF
```

This configuration simply states that we backup `from` our `$HOME` to a backend
labelled `localdir` which we'll define next. Note that both `from` and `to` are
BASH arrays so they can contain more than one entry. This allows us to backup
multiple directories to multiple backends.

For more information on configuring locations see [locations](Configuration/Locations).

## Define a backend

If we run bashtic at this point it'll complain again:

```bash
$ bashtic
No backend found at /home/you/.config/bashtic/backends/localdir
```

We need to configure the backend we referred to as 'localdir' in the location
config above. Let's do that now:

```bash
mkdir -p ~/.config/bashtic/backends
cat > ~/.config/bashtic/backends/localdir << EOF
type="local"
path="/tmp/bashtic_test_repo"
EOF
```

This configuration defines a `local` type of backend with the path
`/tmp/bashtic_test_repo`. So this backend will target a repository at this
location.

Let's create a test repo where our configuration expects it:

```bash
restic init -r /tmp/bashtic_test_repo
```

You'll need to remember the password for the next step but as this is a test it
does not need to be long.

For more information on configuring backends see
[backends](Configuration/Backends).

## Run bashtic

We now ready to run a bashtic backup:

```bash
$ bashtic backup
enter password for repository:
repository 37f039c2 opened successfully, password is correct
created new cache in /home/you/.cache/restic

Files:          75 new,     0 changed,     0 unmodified
Dirs:           42 new,     0 changed,     0 unmodified
Added to the repo: 367.673 KiB

processed 75 files, 314.460 KiB in 0:00
snapshot 1c575225 saved
```

Congratulations this is your first backup with bashtic.

Let's explain what just happened:

```bash
bashtic backup
```

This command asked bashtic to run a pipeline called 'backup'. The is one of the
built-in pipelines included with bashtic. There are two others:

* forget
* check

Go ahead and try these now. If you are familiar with restic you can guess what
they will do. The forget pipeline should be safe as we have not defined any
forget policy yet so restic will do nothing. A forget policy lives in the
location configuration, see [locations](Configuration/Locations) for more on
that.

Let's also try out the dry-run feature:

```bash
$ bashtic --dryrun backup
enter password for repository:
restic backup -r /tmp/bashtic_test_repo /home/you
```

Now bashtic logs the restic commands it would invoke, instead of running them.
This is useful to ensure your configuration is correct without changing your
repositories.

So far all bashtic has done is save us some typing at the cmdline. What else can
we do?

## Create a custom pipeline

Let's imagine that after every backup we want to run a check as well. We could
do this:

```bash
bashtic backup
bashtic check
```

However there's a way to combine these steps in our own custom pipeline. Let's
do that now:

```bash
mkdir -p ~/.config/bashtic/pipelines
cat > ~/.config/bashtic/pipelines/mypipeline << EOF
bashtic_backup
bashtic_check
EOF
```

Here we create a pipeline called `mypipeline` with the config file
`~/.config/bashtic/pipelines/mypipeline`. The content is pure BASH. Bashtic
will run your pipeline in an environment that includes functions for you to call
that invoke the suggested restic operations. In this case we have specified:

```bash
bashtic_backup
bashtic_check
```

This simple pipeline will backup and then check the repo. Let's ask bashtic to
run this pipeline now:

```bash
$ bashtic mypipeline
enter password for repository:
repository 37f039c2 opened successfully, password is correct

Files:           1 new,     0 changed,    75 unmodified
Dirs:            1 new,     6 changed,    36 unmodified
Added to the repo: 7.330 KiB

processed 76 files, 314.488 KiB in 0:00
snapshot bc16b339 saved
using temporary cache in /tmp/restic-check-cache-458583489
repository 37f039c2 opened successfully, password is correct
created new cache in /tmp/restic-check-cache-458583489
create exclusive lock for repository
load indexes
check all packs
check snapshots, trees and blobs
no errors were found
```

Congratulations - now you can create whatever pipelines you need for your
workflow in pure BASH. Besides the `bashtic_backup` and `bashtic_check`
functions we used above you can also call the function `bashtic_forget` to run a
forget operation.

When developing your own pipelines don't forget the dry-run feature to test your
configuration:

```bash
$ bashtic --dryrun mypipeline
enter password for repository:
restic backup -r /tmp/bashtic_test_repo /home/you
restic check -r /tmp/bashtic_test_repo
```

```{caution}
Bashtic can only apply dry-run to its own functions, any custom BASH code in
your pipeline will be executed as normal.
```

If you name your pipeline `default` then you don't even need to name it
explicitly on the cmdline:

```bash
bashtic
```

You can also pass cmdline arguments to your custom pipeline by specifying them
after a `--` separator:

```bash
bashtic -- arg1 arg2
```

There are infinite possibilities for what you put in your own pipelines, here
are some ideas:

* Check filesystems are mounted/unmounted before/after backups.
* Stop system services that write to disk during backup and resume them on
  completion. Or abort the backup process if they can't be interrupted.
* Skip a backup if it's been done recently.
* Warn if a backend has not been backed up to recently.
* Notify a desktop user a backup is underway/completed.

For more information on developing pipelines see [pipelines](Configuration/Pipelines).

## Tidy up

If you've finished trying things out let's get rid of the temporary repo:

```bash
rm -rf /tmp/bashtic_test_repo
```

If you also want to clear out the bashtic configuration we've created run the
following command. Just be mindful if you've made your own alterations to save
them first if needed:

```bash
rm -f ~/.config/bashtic/locations/default \
      ~/.config/bashtic/backends/localdir \
      ~/.config/bashtic/pipelines/mypipeline
```

## Next moves

Thank you for getting this far. Some thoughts on what you might do next:

* Learn more about developing your own [pipelines](Configuration/Pipelines).
* Refine the definition of your [locations](Configuration/Locations) - like
  adding a forget policy.
* Add more [backends](Configuration/Backends) so you can backup to more places.
