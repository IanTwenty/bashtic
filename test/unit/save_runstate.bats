#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# A BASH wrapper for restic
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

bats_require_minimum_version 1.5.0

source "$BATS_TEST_DIRNAME"/../../bashtic
load ../test_helper/bats-support/load
load ../test_helper/bats-assert/load

################################################################################
#
# Tests
#
################################################################################

@test "saving state respects values on load even if overwritten in meantime" {
  declare -g blah=thing
  save_runstate "blah"

  blah=anotherthing
  load_runstate

  assert_equal "$blah" "thing"
}

@test "saves arrays" {
  declare -ga array=("thing")
  save_runstate "array"

  array=("anotherthing")
  load_runstate

  assert_equal "${array[@]}" "thing"
}

@test "saves associative arrays" {
  declare -gA blah=(["one"]="thing")
  save_runstate "blah"

  blah=(["one"]="anotherthing")
  load_runstate

  assert_equal "${!blah[@]}" "one"
  assert_equal "${blah[@]}" "thing"
}

@test "other vars unaffected" {
  declare -g imsaved=thing
  declare -g notme=thing
  save_runstate "imsaved"

  imsaved=anotherthing
  notme=anotherthing
  load_runstate

  assert_equal "$imsaved" "thing"
  assert_equal "$notme" "anotherthing"
}

@test "saving multiple vars" {
  declare -g one=saved
  declare -g two=saved

  save_runstate "one" "two"

  one=notsave
  two=notsave

  load_runstate

  assert_equal "$one" "saved"
  assert_equal "$two" "saved"
}

@test "runstate is what we expect" {
  local _bashtic_runstate_var="_bashtic_$$_RUNSTATE"
  declare -g one=saved
  declare -g two=saved

  save_runstate "one" "two"
  assert_equal "${!_bashtic_runstate_var}" "\
unset one;declare -g -- one=\"saved\";unset two;declare -g -- two=\"saved\""
}

@test "runstate is cleared when requested" {
  local _bashtic_runstate_var="_bashtic_$$_RUNSTATE"
  declare -g one=saved
  declare -g two=saved

  save_runstate "one" "two"
  clear_runstate
  assert_equal "${!_bashtic_runstate_var}" ""
}
