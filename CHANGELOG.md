<!--
SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

A BASH wrapper for restic

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.2](https://gitlab.com/IanTwenty/bashtic/-/releases/v0.0.2) - 2023-06-30

### Added

* We now support `restore` as a default pipeline and `bashtic_restore` as a
  function to call in your own pipelines. You will need to provide `--to` and
  `--snapshot` arguments as restore does not make sense without these. You can
  also pass additional arguments to restore in your own pipelines by adding
  flags to a `restore_flags` array.
* Define system-wide custom pipelines at
  `$XDG_CONFIG_DIRS/bashtic/pipelines` that will be available to all users.
  `XDG_CONFIG_DIRS` defaults to `/etc/xdg` as per XDG spec.
* Pass custom arguments to a pipeline at the cmdline by specifying
  them after a `--` separator, e.g. `bashtic mypipeline -- arg1 arg2`.
* Specify a backend with the `bashtic pipeline@backend` syntax. Pipeline is
  still optional so you can just write `bashtic @backend` to run the default
  pipeline.
* Added missing documentation for `backup_flags` setting on Locations.

### Fixed

* Fixed a bug where error msgs in a user's cludes were output to the tmp file in
  which they are captured, preventing user from seeing them.

## [0.0.1](https://gitlab.com/IanTwenty/bashtic/-/releases/v0.0.1) - 2023-03-03

* First version
