#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# A BASH wrapper for restic
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

bats_require_minimum_version 1.5.0

source "$BATS_TEST_DIRNAME"/../../bashtic
load ../test_helper/bats-support/load
load ../test_helper/bats-assert/load

################################################################################
#
# Tests
#
################################################################################

@test "password and repo exported for restic to use, then torn down" {
  local path="repo"

  # Will prompt as vars not set
  restic_env_setup <<< "password"

  assert_equal "$(export -p | grep " RESTIC.*=")" "\
declare -x RESTIC_PASSWORD=\"password\"
declare -x RESTIC_REPOSITORY=\"repo\""

  restic_env_teardown

  assert_equal "$RESTIC_PASSWORD" ""
  assert_equal "$RESTIC_REPOSITORY" ""
}

@test "export restic env vars if user has set them" {
  local path="repo"
  # you might think the user should export RESTIC_PASSWORD themselves but they
  # cannot as the environment in which we source their location is discarded
  # shellcheck disable=SC2030 # modification is local to the test, fine
  RESTIC_PASSWORD="myown"
  # shellcheck disable=SC2030 # modification is local to the test, fine
  RESTIC_PASSWORD_COMMAND="cmd"
  # shellcheck disable=SC2030 # modification is local to the test, fine
  RESTIC_PASSWORD_FILE="file"

  restic_env_setup

  assert_equal "$(export -p | grep " RESTIC.*=")" "\
declare -x RESTIC_PASSWORD=\"myown\"
declare -x RESTIC_PASSWORD_COMMAND=\"cmd\"
declare -x RESTIC_PASSWORD_FILE=\"file\"
declare -x RESTIC_REPOSITORY=\"repo\""

  restic_env_teardown

  assert_equal "$RESTIC_PASSWORD" ""
  assert_equal "$RESTIC_PASSWORD_COMMAND" ""
  assert_equal "$RESTIC_PASSWORD_FILE" ""
  assert_equal "$RESTIC_REPOSITORY" ""
}

@test "single env var exported if user has set it" {
  local path="repo"
  RESTIC_PASSWORD=myown

  restic_env_setup

  assert_equal "$(export -p | grep " RESTIC.*")" "\
declare -x RESTIC_PASSWORD=\"myown\"
declare -x RESTIC_REPOSITORY=\"repo\""

  restic_env_teardown
  RESTIC_PASSWORD_COMMAND="cmd"

  restic_env_setup

  assert_equal "$(export -p | grep " RESTIC.*")" "\
declare -x RESTIC_PASSWORD_COMMAND=\"cmd\"
declare -x RESTIC_REPOSITORY=\"repo\""

  restic_env_teardown
  RESTIC_PASSWORD_FILE="file"

  restic_env_setup

  assert_equal "$(export -p | grep " RESTIC.*")" "\
declare -x RESTIC_PASSWORD_FILE=\"file\"
declare -x RESTIC_REPOSITORY=\"repo\""

  restic_env_teardown

  # shellcheck disable=SC2031 # modification is local to the test, fine
  assert_equal "$RESTIC_PASSWORD" ""
  assert_equal "$RESTIC_PASSWORD_COMMAND" ""
  assert_equal "$RESTIC_PASSWORD_FILE" ""
  assert_equal "$RESTIC_REPOSITORY" ""
}
