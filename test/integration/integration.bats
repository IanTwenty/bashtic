#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# A BASH wrapper for restic
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

bats_require_minimum_version 1.5.0

# shellcheck disable=SC1091
source "$BATS_TEST_DIRNAME"/../../bashtic
load ../test_helper/bats-support/load
load ../test_helper/bats-assert/load

################################################################################
#
# Tests
#
################################################################################

@test "no location, fails" {
  tmpdir=$(mktemp -d)
  XDG_CONFIG_HOME="$tmpdir" run ! main
  assert_output "No location found at $tmpdir/bashtic/locations/default"
}

@test "one location, no backend, fails" {
  tmpdir=$(mktemp -d)
  mkdir -p "$tmpdir/bashtic/locations"
  cat >  "$tmpdir/bashtic/locations/default" << EOF
to=("nobackend")
EOF

  XDG_CONFIG_HOME="$tmpdir" run ! main
  assert_output "No backend found at $tmpdir/bashtic/backends/nobackend"
}

@test "one location, one backend, no pipeline, fails" {
  tmpdir=$(mktemp -d)
  mkdir -p "$tmpdir/bashtic/locations"
  mkdir -p "$tmpdir/bashtic/backends"
  cat >  "$tmpdir/bashtic/locations/default" << EOF
to=("usb")
EOF
  cat >  "$tmpdir/bashtic/backends/usb" << EOF
type="local"
path="/media/usb"
RESTIC_PASSWORD="password"
EOF

  XDG_CONFIG_HOME="$tmpdir" run ! main badpipeline
  assert_output "No pipeline found. Tried $tmpdir/bashtic/pipelines/badpipeline and /etc/xdg/bashtic/pipelines/badpipeline"
}

@test "one location, one backend, builtin pipeline, succeeds" {
  restic_called="false"
  restic() {
    restic_called="true"
  }
  tmpdir=$(mktemp -d)
  mkdir -p "$tmpdir/bashtic/locations"
  mkdir -p "$tmpdir/bashtic/backends"
  cat >  "$tmpdir/bashtic/locations/default" << EOF
from=("/home")
to=("usb")
EOF
  cat >  "$tmpdir/bashtic/backends/usb" << EOF
type="local"
path="/media/usb"
RESTIC_PASSWORD="password"
EOF

  XDG_CONFIG_HOME="$tmpdir" main backup
  assert "$restic_called"
}

@test "one location, one backend, default pipeline, succeeds" {
  restic() {
    exit 99
  }
  tmpdir=$(mktemp -d)
  mkdir -p "$tmpdir/bashtic/locations"
  mkdir -p "$tmpdir/bashtic/backends"
  mkdir -p "$tmpdir/bashtic/pipelines"
  cat >  "$tmpdir/bashtic/locations/default" << EOF
from=("/home")
to=("usb")
EOF
  cat >  "$tmpdir/bashtic/backends/usb" << EOF
type="local"
path="/media/usb"
RESTIC_PASSWORD="password"
EOF
  cat >  "$tmpdir/bashtic/pipelines/default" << EOF
bashtic_backup
EOF

  # Use exit 99 to hear back from restic function above as it'll be run in a subshell
  XDG_CONFIG_HOME="$tmpdir" run -99 main
}

@test "one location, multiple backends, default pipeline@backend, succeeds" {
  restic() {
    echo "$@"
  }
  tmpdir=$(mktemp -d)
  mkdir -p "$tmpdir/bashtic/locations"
  mkdir -p "$tmpdir/bashtic/backends"
  mkdir -p "$tmpdir/bashtic/pipelines"
  cat >  "$tmpdir/bashtic/locations/default" << EOF
from=("/home")
to=("usb" "extdrive")
EOF
  cat >  "$tmpdir/bashtic/backends/usb" << EOF
type="local"
path="/media/usb"
RESTIC_PASSWORD="password"
EOF
  cat >  "$tmpdir/bashtic/backends/extdrive" << EOF
type="local"
path="/media/ext"
RESTIC_PASSWORD="password"
EOF
  cat >  "$tmpdir/bashtic/pipelines/default" << EOF
bashtic_backup
EOF

  XDG_CONFIG_HOME="$tmpdir" run main @extdrive
  assert_output "backup -r /media/ext /home"
}

@test "backend settings are independent" {
  tmpfile=$(mktemp)
  restic() {
    { echo "$@" ; 
      echo "$RESTIC_PASSWORD" ; 
      echo "$RESTIC_PASSWORD_FILE" ; } >> "$tmpfile"
  }
  tmpdir=$(mktemp -d)
  mkdir -p "$tmpdir/bashtic/locations"
  mkdir -p "$tmpdir/bashtic/backends"
  mkdir -p "$tmpdir/bashtic/pipelines"
  cat >  "$tmpdir/bashtic/locations/default" << EOF
from=("/home")
to=("usb1" "usb2")
EOF
  cat >  "$tmpdir/bashtic/backends/usb1" << EOF
type="local"
path="/media/usb1"
RESTIC_PASSWORD="password"
EOF
  cat >  "$tmpdir/bashtic/backends/usb2" << EOF
type="local"
path="/media/usb2"
RESTIC_PASSWORD_FILE="file"
EOF
  cat >  "$tmpdir/bashtic/pipelines/default" << EOF
bashtic_backup
EOF

  XDG_CONFIG_HOME="$tmpdir" main

  run cat "$tmpfile"
  assert_output "\
backup -r /media/usb1 /home
password

backup -r /media/usb2 /home

file"
}

@test "dryrun mode" {
  tmpdir=$(mktemp -d)
  mkdir -p "$tmpdir/bashtic/locations"
  mkdir -p "$tmpdir/bashtic/backends"
  cat >  "$tmpdir/bashtic/locations/default" << EOF
from=("/home")
to=("usb")
EOF
  cat >  "$tmpdir/bashtic/backends/usb" << EOF
type="local"
path="/media/usb"
RESTIC_PASSWORD="password"
EOF

  XDG_CONFIG_HOME="$tmpdir" run main -n backup
  assert_output "restic backup -r /media/usb /home"

  XDG_CONFIG_HOME="$tmpdir" run main --dryrun backup
  assert_output "restic backup -r /media/usb /home"
}

@test "restore cmdline argument handling" {
  tmpdir=$(mktemp -d)
  system_tmpdir=$(mktemp -d)
  mkdir -p "$tmpdir/bashtic/locations"
  mkdir -p "$tmpdir/bashtic/backends"
  cat >  "$tmpdir/bashtic/locations/default" << EOF
from=("/home")
to=("usb")
EOF
  cat >  "$tmpdir/bashtic/backends/usb" << EOF
type="local"
path="/media/usb"
RESTIC_PASSWORD="password"
EOF

  XDG_CONFIG_DIRS="$system_tmpdir" XDG_CONFIG_HOME="$tmpdir" run main restore -t
  assert_failure ; assert_output "ERROR: 'to' flag specified but no dir provided."
  XDG_CONFIG_DIRS="$system_tmpdir" XDG_CONFIG_HOME="$tmpdir" run main restore -s
  assert_failure ; assert_output "ERROR: 'snapshot' flag specified but no snapshot provided."
  XDG_CONFIG_DIRS="$system_tmpdir" XDG_CONFIG_HOME="$tmpdir" run main -n restore -t / -s latest
  assert_success ; assert_output "restic restore -r /media/usb latest --target /"
}

@test "pipeline that accidentally clobbers every var" {
  tmpdir=$(mktemp -d)
  mkdir -p "$tmpdir/bashtic/locations"
  mkdir -p "$tmpdir/bashtic/backends"
  mkdir -p "$tmpdir/bashtic/pipelines"
  cat >  "$tmpdir/bashtic/locations/default" << EOF
from=("/home")
to=("usb")
EOF
  cat >  "$tmpdir/bashtic/backends/usb" << EOF
type="local"
path="/media/usb"
RESTIC_PASSWORD="password"
EOF
cat >  "$tmpdir/bashtic/pipelines/default" << EOF
unset from
unset to
unset type
unset path
unset RESTIC_PASSWORD
bashtic_backup
EOF

  XDG_CONFIG_HOME="$tmpdir" run main -n backup
  assert_output "restic backup -r /media/usb /home"
}

@test "pass extended args to custom pipelines" {
  tmpdir=$(mktemp -d)
  mkdir -p "$tmpdir/bashtic/locations"
  mkdir -p "$tmpdir/bashtic/backends"
  mkdir -p "$tmpdir/bashtic/pipelines"
  cat >  "$tmpdir/bashtic/locations/default" << EOF
from=("/home")
to=("usb")
EOF
  cat >  "$tmpdir/bashtic/backends/usb" << EOF
type="local"
path="/media/usb"
RESTIC_PASSWORD="password"
EOF
  cat >  "$tmpdir/bashtic/pipelines/default" << EOF
  echo "\$@"
EOF

  XDG_CONFIG_HOME="$tmpdir" run main -- extended arguments
  assert_output "extended arguments"
}
