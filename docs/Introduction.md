<!--
SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

A BASH wrapper for restic

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Introduction

Bashtic is a BASH wrapper for restic. It's modelled on the excellent
[autorestic](https://github.com/cupcakearmy/autorestic) but uses pure BASH for
the configuration and definition of your restic workflows.

```{caution}
Bashtic is currently in early development and should be used with caution.
```

## Features

* Define custom workflows ([pipelines](Configuration/Pipelines)) in pure BASH.
  Bashtic provides BASH functions for you to call when you need a restic
  operation like backup, check and forget. This is equivalent to having your own
  hooks around any operation but you completely control the flow. For a
  simple example see [create a custom
  pipeline](./QuickStart.md#create-a-custom-pipeline) in our [quick
  start](QuickStart) guide.
* Configure restic [locations](Configuration/Locations),
  [backends](Configuration/Backends) with standard BASH variables and arrays.
  These settings are passed seamlessly to the restic operations in your
  pipelines.
* Use the dry-run mode (`--dryrun|-n`) to see the full restic commands that
  would be run during a pipeline. Helps you check your config before committing
  to it.
* Nest include/exclude rules to any depth with our
  [cludes](Cludes) feature - avoiding issues around the use of the `files-from`
  flag with restic.

Check out the [roadmap](Roadmap) to see what's coming up.

## Current restrictions

Bashtic is new and is missing some features you might expect:

* We only provide support for the restic operations of backup, check and forget,
  though you can call any restic command in your own pipelines.
* We only support local restic repositories as a
  [backend](Configuration/Backends) type.
* Only one [location](Configuration/Locations) (backup source definition) can be
  defined.
* Whilst you can define multiple [backends](Configuration/Backends) for a
  [location](Configuration/Locations) you cannot choose which one to use during
  a pipeline run - bashtic will iterate through all of them.

Ready to go? Dive into the [quick start](QuickStart).
