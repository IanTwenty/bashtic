<!--
SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

A BASH wrapper for restic

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# bashtic

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![Documentation Status](https://readthedocs.org/projects/bashtic/badge/?version=latest&style=flat-square)](https://bashtic.readthedocs.io/en/latest/?badge=latest)

A BASH wrapper for restic. Easy to install with configurable restic
pipelines in pure BASH and advanced rules for file inclusion/exclusion.

**NOTE** bashtic is currently in early development and should be used with
caution.

Features:

* Define custom workflows (pipelines) in pure BASH.
  Bashtic provides BASH functions for you to call when you need a restic
  operation like backup, check and forget. This is equivalent to having your own
  hooks around any operation but you completely control the flow. For a
  simple example see [create a custom
  pipeline](https://bashtic.readthedocs.io/en/latest/QuickStart.html#create-a-custom-pipeline)
  in our [quick start](https://bashtic.readthedocs.io/en/latest/QuickStart.html)
  guide.
* Configure restic locations,
  backends with standard BASH variables and arrays.
  These settings are passed seamlessly to the restic operations in your
  pipelines.
* Use the dry-run mode (`--dryrun|-n`) to see the full restic commands that
  would be run during a pipeline. Helps you check your config before committing
  to it.
* Nest include/exclude rules to any depth with our cludes feature - avoiding
  issues around the use of the `files-from` flag with restic.

Full documentation is available at
[bashtic.readthedocs.io](https://bashtic.readthedocs.io/en/latest/?badge=latest).
The official repository is
[bashtic](https://gitlab.com/IanTwenty/bashtic).

## Table of Contents

<!-- vim-markdown-toc GitLab -->

* [Background](#background)
* [Install](#install)
  * [Via bin](#via-bin)
  * [Via git clone](#via-git-clone)
* [Usage](#usage)
  * [Initial configuration](#initial-configuration)
  * [First run](#first-run)
* [Current restrictions](#current-restrictions)
* [Contributing](#contributing)
* [License](#license)

<!-- vim-markdown-toc -->

## Background

The excellent [autorestic](https://github.com/cupcakearmy/autorestic) did not
quite meet my requirements:

* I want my backup process to be a custom sequence of restic operations - not
  just a backup but also forget and check.
* I want to add my own hooks to any stage of the process.

In addition I wanted to be able to nest includes/excludes however I want and at
any depth.

I loved the way autorestic was configured I just needed slightly more
flexibility and wanted to use all BASH.

## Install

There are two ways:

### Via bin

**RECOMMENDED** Install with [bin](https://github.com/marcosnils/bin):

```bash
bin install https://gitlab.com/IanTwenty/bashtic
```

bin will allow you to manage the install and update in future.

### Via git clone

Git clone the repository and copy bashtic onto your path. For example:

```bash
git clone https://gitlab.com/IanTwenty/bashtic
cd bashtic
cp bashtic /usr/local/bin
```

## Usage

Below is a short introduction. For more information our full documentation is
available at
[bashtic.readthedocs.io](https://bashtic.readthedocs.io/en/latest/?badge=latest).
We recommend the [Quick
Start](https://bashtic.readthedocs.io/en/latest/QuickStart.html) for your first
steps.

### Initial configuration

We will be brief here and use a test repository for your first run. First
create a default location, backing up your home dir to a 'localdir' backend:

```bash
mkdir -p ~/.config/bashtic/locations
cat > ~/.config/bashtic/locations/default << EOF
from=("$HOME")
to=("localdir")
EOF
```

Next configure the 'localdir' backend we have just referred to:

```bash
mkdir -p ~/.config/bashtic/backends
cat > ~/.config/bashtic/backends/localdir << EOF
type="local"
path="/tmp/bashtic_test_repo"
EOF
```

Create the test repo where our configuration expects it:

```bash
restic init -r /tmp/bashtic_test_repo
```

You'll need to remember the password for the next step but as this is a test it
does not need to be long.

### First run

First check your configuration by running in dryrun mode with the `--dryrun` or
`-n` flag. This will show you what restic commands bashtic would run:

```bash
bashtic --dryrun backup
```

If that looks ok you can drop the `--dryrun` to do a real backup.

Above we choose to run the built-in 'backup' pipeline. Bashtic comes with other
built-in pipelines for the common restic operations:

```bash
bashtic backup
bashtic check
bashtic forget
```

Most of bashtic's power however is in developing your own custom pipelines which
are just plain BASH files under ` ~/.config/bashtic/pipelines`. To run one you
have created yourself simply give its name:

```bash
bashtic mypipeline
```

Or you can create a pipeline called `default` and this will be run if no other
is specified on the command line.

To learn more about making your own pipelines see [create a custom
pipeline](https://bashtic.readthedocs.io/en/latest/QuickStart.html#create-a-custom-pipeline)
in our [Quick Start](https://bashtic.readthedocs.io/en/latest/QuickStart.html)
guide.

Read more about all this at [bashtic.readthedocs.io](https://bashtic.readthedocs.io/en/latest/?badge=latest).

## Current restrictions

Bashtic is new and is missing some features you might expect:

* We only provide support for the restic operations of backup, check and forget,
  though you can call any restic command in your own pipelines.
* We only support local restic repositories as a backend type.
* Only one location (backup source definition) can be defined.
* Whilst you can define multiple backeds for a location you cannot choose which
  one to use during a pipeline run - bashtic will iterate through all of them.

Check out the [roadmap](https://bashtic.readthedocs.io/en/latest/Roadmap.html)
to see what's coming up.

## Contributing

It's great that you're interested in contributing. Please ask questions by
raising an issue and PRs will be considered. For full details see
[CONTRIBUTING.md](CONTRIBUTING.md)

## License

We declare our licensing by following the REUSE specification - copies of
applicable licenses are stored in the LICENSES directory. Here is a summary:

* Source code is licensed under GPL-3.0-or-later.
* Anything else that is not executable, including the text when extracted from
  code, is licensed under CC-BY-SA-4.0.
* Where we use a range of copyright years it is inclusive and shorthand for
  listing each year individually as a copyrightable year in its own right.

For more accurate information, check individual files.

Bashtic is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.
