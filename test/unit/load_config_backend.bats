#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# A BASH wrapper for restic
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

bats_require_minimum_version 1.5.0

source "$BATS_TEST_DIRNAME"/../../bashtic
load ../test_helper/bats-support/load
load ../test_helper/bats-assert/load

################################################################################
#
# Tests
#
################################################################################

@test "type must be specified" {
  config=""

  run ! load_config_backend <(echo "$config")

  assert_output --regexp "Backend .*: 'type' is missing but it's not optional!"
}

@test "path must be specified" {
  config="type='local'"

  run ! load_config_backend <(echo "$config")

  assert_output --regexp "Backend .*: 'path' is missing but it's not optional!"
}

@test "restic env password vars are respected" {
  config="type='local';path='/opt/backup';\
  RESTIC_PASSWORD=\"password\";
  RESTIC_PASSWORD_COMMAND=\"command\";
  RESTIC_PASSWORD_FILE=\"file\""

  load_config_backend <(echo "$config")
  assert_equal "$RESTIC_PASSWORD" "password"
  assert_equal "$RESTIC_PASSWORD_COMMAND" "command"
  assert_equal "$RESTIC_PASSWORD_FILE" "file"
}

@test "valid config sets runstate var" {
  local _bashtic_runstate_var="_bashtic_$$_RUNSTATE"
  config="type='local';path='/opt/backup'"

  load_config_backend <(echo "$config")

  assert_equal "${!_bashtic_runstate_var}" "\
unset type;declare -g -- type=\"local\";\
unset path;declare -g -- path=\"/opt/backup\""
}

@test "extra config ignored" {
  local _bashtic_runstate_var="_bashtic_$$_RUNSTATE"
  blah=safe
  config="type='local';path='/opt/backup';blah=changed"

  load_config_backend <(echo "$config")

  assert_equal "$blah" "safe"
}
