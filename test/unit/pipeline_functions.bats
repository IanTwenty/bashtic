#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# A BASH wrapper for restic
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

bats_require_minimum_version 1.5.0

source "$BATS_TEST_DIRNAME"/../../bashtic
load ../test_helper/bats-support/load
load ../test_helper/bats-assert/load

################################################################################
#
# Tests
#
################################################################################

@test "respect dryrun mode" {
  path="/home"
  bashtic_dryrun="true"

  run bashtic_backup
  assert_output "restic backup -r /home"
  run bashtic_check
  assert_output "restic check -r /home"
  run bashtic_forget
  assert_output "restic forget -r /home"
}

@test "respect dryrun mode from runstate" {
  path="/home"
  bashtic_dryrun="true"
  save_runstate "bashtic_dryrun"

  # As if user accidentally overrode one of our settings
  bashtic_dryrun="false"

  run bashtic_backup
  assert_output "restic backup -r /home"
  run bashtic_check
  assert_output "restic check -r /home"
  run bashtic_forget
  assert_output "restic forget -r /home"
}
