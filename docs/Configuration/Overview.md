<!--
SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

A BASH wrapper for restic

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Overview

Bashtic looks for configuration in `$XDG_CONFIG_DIR/bashtic`. If
`$XDG_CONFIG_DIR` is not defined it defaults to `$HOME/.config`.

Location, backend and pipeline configuration files are kept in directories
underneath:

```bash
$XDG_CONFIG_DIR/bashtic/locations
$XDG_CONFIG_DIR/bashtic/backends
$XDG_CONFIG_DIR/bashtic/pipelines
```

You can have both a default location and a default pipeline which will be used
by bashtic if no other is specified on the command line. These should be defined
in the files:

```bash
$XDG_CONFIG_DIR/bashtic/locations/default
$XDG_CONFIG_DIR/bashtic/pipelines/default
```

These could also be symlinks.

## Loading configuration

The configuration files will be BASH `source`d (.) by bashtic. The values of
expected configuration variables will be saved for later use by the bashtic
pipelines and functions. Any variables prefixed with `custom_` will also be let
through to later stages. Anything else is discarded.

You can add any BASH commands you like to the configuration files however it
would seem best to keep locations and backends clear of complex logic and save
that for your pipelines.

## Next

To understand more about configuration please keep reading:

* [Locations](Locations)
* [Backends](Backends)
* [Pipelines](Pipelines)
