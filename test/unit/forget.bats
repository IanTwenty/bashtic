#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# A BASH wrapper for restic
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

bats_require_minimum_version 1.5.0

source "$BATS_TEST_DIRNAME"/../../bashtic
load ../test_helper/bats-support/load
load ../test_helper/bats-assert/load
load ../test_helper/bats-mock/load

restic() {
  $restic_cmd "$@"
}

setup_file() {
  restic_cmd="$(mock_create)"
  export restic_cmd
  export -f restic
}

################################################################################
#
# Tests
#
################################################################################

@test "if forget fails we fail" {
  mock_set_status "$restic_cmd" 1
  run ! bashtic_forget
}

@test "forget uses all the 'keep' vars we pass" {
  tmpfile=$(mktemp)
  mock_set_status "$restic_cmd" 0
  mock_set_side_effect "$restic_cmd" "echo "\$@" > $tmpfile"
  local path="repo"
  local -A forget

  # One at a time

  forget=( ["keep-last"]="99"); bashtic_forget
  assert_equal "$(cat "$tmpfile")" "forget -r repo --keep-last 99"

  forget=( ["keep-hourly"]="99"); bashtic_forget
  assert_equal "$(cat "$tmpfile")" "forget -r repo --keep-hourly 99"

  forget=( ["keep-daily"]="99"); bashtic_forget
  assert_equal "$(cat "$tmpfile")" "forget -r repo --keep-daily 99"

  forget=( ["keep-weekly"]="99"); bashtic_forget
  assert_equal "$(cat "$tmpfile")" "forget -r repo --keep-weekly 99"

  forget=( ["keep-monthly"]="99"); bashtic_forget
  assert_equal "$(cat "$tmpfile")" "forget -r repo --keep-monthly 99"

  forget=( ["keep-yearly"]="99"); bashtic_forget
  assert_equal "$(cat "$tmpfile")" "forget -r repo --keep-yearly 99"

  # Combinations

  forget=( ["keep-last"]="1" ["keep-hourly"]="2"); bashtic_forget
  assert_equal "$(cat "$tmpfile")" "forget -r repo --keep-last 1 --keep-hourly 2"

  forget=( ["keep-daily"]="1" ["keep-weekly"]="2"); bashtic_forget
  assert_equal "$(cat "$tmpfile")" "forget -r repo --keep-daily 1 --keep-weekly 2"

  forget=( ["keep-monthly"]="1" ["keep-yearly"]="2"); bashtic_forget
  assert_equal "$(cat "$tmpfile")" "forget -r repo --keep-monthly 1 --keep-yearly 2"

  # Everything, everywhere all at once

  forget=( ["keep-last"]="1" ["keep-hourly"]="2"
           ["keep-daily"]="3" ["keep-weekly"]="4"
           ["keep-monthly"]="5" ["keep-yearly"]="6"); bashtic_forget
  assert_equal "$(cat "$tmpfile")" \
               "forget -r repo \
--keep-last 1 --keep-hourly 2 \
--keep-daily 3 --keep-weekly 4 \
--keep-monthly 5 --keep-yearly 6"
}

@test "forget loads vars from runstate" {
  tmpfile=$(mktemp)
  mock_set_status "$restic_cmd" 0
  mock_set_side_effect "$restic_cmd" "echo "\$@" > $tmpfile"
  local path="repo"
  local -A forget
  forget=( ["keep-last"]="1" ["keep-hourly"]="2"
           ["keep-daily"]="3" ["keep-weekly"]="4"
           ["keep-monthly"]="5" ["keep-yearly"]="6")
  save_runstate "path" "forget"

  # Attempt to override vars
  path="nope";forget=( ["nope"]="nope")

  bashtic_forget

  assert_equal "$(cat "$tmpfile")" \
               "forget -r repo \
--keep-last 1 --keep-hourly 2 \
--keep-daily 3 --keep-weekly 4 \
--keep-monthly 5 --keep-yearly 6"
}

@test "forget respects user's custom flags" {
  tmpfile=$(mktemp)
  mock_set_status "$restic_cmd" 0
  mock_set_side_effect "$restic_cmd" "echo "\$@" > $tmpfile"
  local path="repo"

  local -a forget_flags;forget_flags=("--quiet" "--prune"); bashtic_forget

  assert_equal "$(cat "$tmpfile")" "forget -r repo --quiet --prune"
}
