<!--
SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

A BASH wrapper for restic

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Roadmap

* Release next v0
  * We should print out all our settings before each run for logging etc info cmd
    or provide command for this or a verbosity setting.
  * Support all exclude/include flags of restic
  * Allow multiple locations. Need to be sure we clear state between locations
    correctly.
* Release v1
  * Support more/all backend types
  * More default pipelines and functions for user pipelines: check, snapshots etc

The future:

* Location should probably set its default pipeline as some pipelines may not be
  suitable for every location. Or a list of allowed pipelines, with one default.
  So user can't run wrong pipeline against wrong location.
* Could we have a check pipeline or change our checks as part of daily backup to
  check a rotating/progressive subset of the backup. Maybe introduce
  `check_flags` for this.
* It seems weird you have to provide location and backend just to do a restore.
  Perhaps allow setting repository path on cmdline for that?
