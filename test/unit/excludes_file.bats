#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# A BASH wrapper for restic
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

bats_require_minimum_version 1.5.0

source "$BATS_TEST_DIRNAME"/../../bashtic
load ../test_helper/bats-support/load
load ../test_helper/bats-assert/load

setup_file() {
  # Shared vars must be global and exported to survive to test runs
  # https://bats-core.readthedocs.io/en/stable/tutorial.html#avoiding-costly-repeated-setups
  declare -g testdir;testdir=$(mktemp -d)
  export testdir

  # Let's make something that looks like a user home
  touch "$testdir/.bash_history"
  touch "$testdir/.bash_logout"
  touch "$testdir/.bash_profile"
  touch "$testdir/.bashrc"
  mkdir "$testdir/.config"
  mkdir "$testdir/.config/mdl"
  mkdir "$testdir/.local"
  mkdir "$testdir/.local/evolution"
  mkdir "$testdir/.local/evolution/data"
  mkdir "$testdir/.gitlab"
  touch "$testdir/.gitlab/config"
  touch "$testdir/.gitlab/filea"
  touch "$testdir/.gitlab/fileb"
  touch "$testdir/.gitlab/filec"
  mkdir "$testdir/.gitlab-extras"
  mkdir "$testdir/Desktop"
  mkdir "$testdir/Documents"
}

teardown_file() {
  rm -rf "$testdir"
}

################################################################################
#
# Top-level shenanigans
#
################################################################################

@test "include at the very root" {
  cludes=(
    "$testdir#i"
    "$testdir/.bash_history#x"
    "$testdir/.bashrc#i"
  )
  run excludes_file

  assert_output "$testdir/.bash_history"
}

@test "exclude at the very root" {
  cludes=(
    "$testdir#x"
    "$testdir/.bash_history#x"
    "$testdir/.bashrc#i"
  )
  run excludes_file

  assert_output "\
$testdir/*
!$testdir/.bashrc"
}

################################################################################
#
# Ordering
#
################################################################################

@test "cludes out of order" {
  cludes=(
    "$testdir/.config/mdl#i"
    "$testdir/.config#x"
  )
  run excludes_file

  assert_output "\
$testdir/.config/*
!$testdir/.config/mdl"
}

################################################################################
#
# Input checking
#
################################################################################

@test "no wildcards" {
  cludes=("$testdir/*#i") ; run --separate-stderr excludes_file
  assert_failure ; assert_equal "ERROR: No wildcards allowed in cludes: $testdir/*#i" "${stderr_lines[0]}"
  cludes=("$testdir/?#i") ; run --separate-stderr excludes_file
  assert_failure ; assert_equal "ERROR: No wildcards allowed in cludes: $testdir/?#i" "${stderr_lines[0]}"
  cludes=("$testdir/[#i") ; run --separate-stderr excludes_file
  assert_failure ; assert_equal "ERROR: No wildcards allowed in cludes: $testdir/[#i" "${stderr_lines[0]}"
  cludes=("$testdir/]#i") ; run --separate-stderr excludes_file
  assert_failure ; assert_equal "ERROR: No wildcards allowed in cludes: $testdir/]#i" "${stderr_lines[0]}"
}

@test "cludes must end in a directive we understand" {
  cludes=("$testdir/thing") ; run --separate-stderr excludes_file
  assert_failure ; assert_equal "ERROR: Cludes must end with an include/exclude directive: $testdir/thing" "${stderr_lines[0]}"
  cludes=("$testdir/thing#p") ; run --separate-stderr excludes_file
  assert_failure ; assert_equal "ERROR: Cludes must end with an include/exclude directive: $testdir/thing#p" "${stderr_lines[0]}"
}

@test "cludes must be absolute" {
  cludes=("thing") ; run --separate-stderr excludes_file
  assert_failure ; assert_equal "ERROR: Cludes must be absolute paths: thing" "${stderr_lines[0]}"
}

################################################################################
#
# Includes inside excludes
#
################################################################################

@test "include a dir inside an excluded dir" {
  cludes=(
    "$testdir/.config#x"
    "$testdir/.config/mdl#i"
  )
  run excludes_file

  assert_output "\
$testdir/.config/*
!$testdir/.config/mdl"
}

@test "include a dir inside an excluded dir but its one level deeper" {
  cludes=(
    "$testdir/.local#x"
    "$testdir/.local/share/evolution#i"
  )
  run excludes_file

  assert_output "\
$testdir/.local/*
!$testdir/.local/share
$testdir/.local/share/*
!$testdir/.local/share/evolution"
}

@test "include a dir inside an excluded dir but its two levels deeper" {
  cludes=(
    "$testdir/.local#x"
    "$testdir/.local/share/evolution/data#i"
  )
  run excludes_file

  assert_output "\
$testdir/.local/*
!$testdir/.local/share
$testdir/.local/share/*
!$testdir/.local/share/evolution
$testdir/.local/share/evolution/*
!$testdir/.local/share/evolution/data"
}

@test "include a two dirs inside an excluded dir but they are two levels deeper" {
  cludes=(
    "$testdir/.local#x"
    "$testdir/.local/share/evolution/data#i"
    "$testdir/.local/share/evolution/state#i"
  )
  run excludes_file

  assert_output "\
$testdir/.local/*
!$testdir/.local/share
$testdir/.local/share/*
!$testdir/.local/share/evolution
$testdir/.local/share/evolution/*
!$testdir/.local/share/evolution/data
!$testdir/.local/share/evolution/state"
}

################################################################################
#
# Excludes inside includes
#
################################################################################

@test "exclude a dir inside an included dir" {
  cludes=(
    "$testdir/.config#x"
  )
  run excludes_file

  assert_output "\
$testdir/.config"
}

@test "exclude two dirs inside an included dir, one shares a prefix with the other" {
  cludes=(
    "$testdir/.gitlab#x"
    "$testdir/.gitlab-extras#x"
  )
  run excludes_file

  assert_output "\
$testdir/.gitlab
$testdir/.gitlab-extras"
}

################################################################################
#
# Include/exclude Files within a dir that's the opposite
#
################################################################################

@test "include file in excluded dir" {
  cludes=(
    "$testdir/.gitlab#x"
    "$testdir/.gitlab/config#i"
  )
  run excludes_file

  assert_output "\
$testdir/.gitlab/*
!$testdir/.gitlab/config"
}

@test "include file in excluded dir, that also has deeper included dirs" {
  cludes=(
    "$testdir/.gitlab#x"
    "$testdir/.gitlab/fileb#i"
    "$testdir/.gitlab/extras#i"
  )
  run excludes_file

  assert_output "\
$testdir/.gitlab/*
!$testdir/.gitlab/extras
!$testdir/.gitlab/fileb"
}

@test "exclude file in included dir" {
  cludes=(
    "$testdir/.gitlab/config#x"
  )
  run excludes_file

  assert_output "\
$testdir/.gitlab/config"
}

################################################################################
#
# Jumping around
#
################################################################################

@test "jump to another included dir at the end" {
  cludes=(
    "$testdir/.local#x"
    "$testdir/.local/share/evolution/data#i"
    "$testdir/Desktop#i"
  )
  run excludes_file

  assert_output "\
$testdir/.local/*
!$testdir/.local/share
$testdir/.local/share/*
!$testdir/.local/share/evolution
$testdir/.local/share/evolution/*
!$testdir/.local/share/evolution/data"
}

@test "jump to another excluded dir at the end" {
  cludes=(
    "$testdir/.local#x"
    "$testdir/.local/share/evolution/data#i"
    "$testdir/Desktop#x"
  )
  run excludes_file

  assert_output "\
$testdir/.local/*
!$testdir/.local/share
$testdir/.local/share/*
!$testdir/.local/share/evolution
$testdir/.local/share/evolution/*
!$testdir/.local/share/evolution/data
$testdir/Desktop"
}
