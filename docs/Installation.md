<!--
SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

A BASH wrapper for restic

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Installation

There are two ways:

## Via bin

**RECOMMENDED** Install with [bin](https://github.com/marcosnils/bin):

```bash
bin install https://gitlab.com/IanTwenty/bashtic
```

bin will allow you to manage the install and update it in future.

## Via git clone

Git clone the repository and copy bashtic onto your path. I recommend you use an
official tag, for example:

```bash
git clone https://gitlab.com/IanTwenty/bashtic --branch TAG
cd bashtic
cp bashtic /usr/local/bin
```

Choose a TAG from our
[releases](https://gitlab.com/IanTwenty/bashtic/releases).

You'll need to manually update and re-copy bashtic to get updates.
