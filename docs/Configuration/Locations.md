<!--
SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

A BASH wrapper for restic

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Locations

Locations must define:

* What to backup `from`
* Which backend to backup `to`

Optionally they can also define:

* A forget policy
* Forget flags to pass to restic forget (e.g. prune)
* Cludes (advanced include/exclude policy)

Location configs are just files under the dir:

```bash
$XDG_CONFIG_DIR/bashtic/locations
```

Currently we only support one location which should be called `default`.

## Backup `from`

The `from` array let's you define what to backup:

```bash
from=("$HOME")
```

To specify multiple directories:

```bash
from=("$HOME" "/opt/gamesaves")
```

The value of `from` is passed directly to restic as the arguments for the
`backup` command.

```{note}
It could be valid to have an empty `from` if you are using other means to
specify what to backup (e.g. `--exclude-file`).
```

## Backup `to`

The `to` array specifies which backends this location can be backed up to:

```bash
to=("usb")
```

Backends are specified by user-defined labels, they can be whatever you need.

To specify more than one:

```bash
to=("usb" "cloud")
```

Whatever labels you use will need to be defined as [backends](Backends).

## `backup_flags`

The `backup_flags` array can be used to specify additional flags to pass to the
restic backup operation. For example to use verbose mode and turn off change
detection we might use:

```bash
backup_flags=("--verbose" "--force")
```

These flags are passed as-is to restic so you can use anything it will understand.

## `forget` policy

The `forget` associative array defines the forget policy for a location:

```bash
forget=(
  ["keep-last"]="10"
  ["keep-hourly"]="5"
  ["keep-daily"]="30"
  ["keep-weekly"]="8"
  ["keep-monthly"]="24"
  ["keep-yearly"]="100"
)
```

At the moment we only support the keys above. You don't have to specify every
key, only those that you want to use. So this is equally valid:

```bash
forget=(
  ["keep-last"]="99"
)
```

## `forget_flags`

The `forget_flags` array can be used to specify additional flags to pass to the
restic forget operation. For example to use quiet mode, enable prune and group
by tags we might use:

```bash
forget_flags=("-q" "--prune" "--group-by" "tags")
```

These flags are passed as-is to restic so you can use anything it will understand.

## `cludes` include/exclude rules

The `cludes` array allows you to specify complex include/exclude rules at any
depth. This is more powerful than anything that can be achieved with restic's
various other inclusion/exclusion flags.

If you want to know more about the background of this feature see
[cludes](../Cludes). However you do not need to understand the background in
order to use it. In brief bashtic will take your `cludes` config, generate a
carefully crafted exclude file and pass it to restic backup with the
`--exclude-file` flag.

By default all files under the paths specified in your `from` array are
considered included. For example to exclude a few files and dirs from our $HOME
we could specify:

```bash
cludes=(
"$HOME/.bash_history#x"
"$HOME/.lesshst#x"
"$HOME/Downloads#x"
"$HOME/bin#x"
)
```

Each entry in the `cludes` array is simply a path with one of two suffixes:

* `#x` - to eXclude the path recursively.
* `#i` - to Include the path recursively.

No wildcards are allowed in paths (for the moment).

Rules can be nested. For example, if we are a flatpak user we might want to
ignore everything under `.var` except for certain dirs with config/data:

```bash
cludes=(
"$HOME/.var#x"
"$HOME/.var/app/net.minetest.Minetest/.minetest/worlds#i"
)
```

The above config will backup any [Minetest](https://www.minetest.net/) worlds
but ignore everything else under `.var`.

You can nest these rules however you like, repeatedly including/excluding at any
depths. For example, if we wanted to exclude a particular Minetest world from
those we backed up above we could add another deeper rule:

```bash
cludes=(
"$HOME/.var#x"
"$HOME/.var/app/net.minetest.Minetest/.minetest/worlds#i"
"$HOME/.var/app/net.minetest.Minetest/.minetest/worlds/MineClone2#x"
)
```

Now we back up all our Minetest worlds except for our
[MineClone2](https://content.minetest.net/packages/Wuzzy/mineclone2/) one.
