<!--
SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

A BASH wrapper for restic

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Pipelines

Pipelines are just BASH script files under the dir:

```bash
$XDG_CONFIG_DIR/bashtic/pipelines
```

## Built-in pipelines

Bashtic comes with built-in pipelines that cover the basic restic commands:

* backup
* check
* forget
* restore

These can be invoked simply:

```bash
bashtic backup
bashtic check
bashtic forget
bashtic restore --to DIR --snapshot ID
```

Note a restore requires the `to` and `snapshot` arguments to make any sense.

## The default pipeline

The special pipeline name of `default` acts as the default pipeline if you run
bashtic without specifying one:

```bash
bashtic
```

This will run the pipeline at `~/.config/bashtic/pipelines/default`.

## Custom pipelines

To create your own pipeline simply create a file in the following dir:

```bash
~/.config/bashtic/pipelines
```

The filename is the name of your pipeline. So if we create an empty pipeline:

```bash
touch ~/.config/bashtic/pipelines/mypipeline
```

We can then run it:

```bash
bashtic mypipeline
```

Bashtic is designed to give you as much control as possible to create your own
workflows. To understand what's possible we'll describe the manner in which we
launch your pipelines from bashtic:

* The BASH Environment
* Safety
* Error handling
* Examples

### BASH Environment

Your pipeline will inherit all the variables set in your current location and
backend configuration e.g. `to`, `from`, `path` etc. See
[configuration](index) to explore them.

Environment variables that restic uses will be appropriately set based on your
current location and backend configuration. This allows you to script your own
calls to restic if needed and have it automatically pick up the right settings.
The exact list of variables set is:

* `RESTIC_REPOSITORY`
* `RESTIC_PASSWORD`
* `RESTIC_PASSWORD_COMMAND`
* `RESTIC_PASSWORD_FILE`

Bashtic makes several BASH functions available to you to perform typical restic
operations:

* `bashtic_backup`
* `bashtic_check`
* `bashtic_forget`
* `bashtic_restore`

All the variables set in your location and backend configuration will also be
available in your environment e.g. `to`, `from` etc. However you do not need to
worry about overwriting them (see [safety](#safety) below).

Seperately any variables with the prefix `custom_` that are assigned in your
location and backend configuration files will also be available in your pipeline.

The `bashtic_restore` function allows you to specify additional flags to pass to
the restic restore cmd, specify them in the `restore_flags` array, e.g.

```bash
restore_flags+=("--include" "$HOME/.*")                # Restore dotfiles
restore_flags+=("--include" "$HOME/Pictures/*")        # Restore pictures
bashtic_restore
```

### Safety

To prevent accidents when you call a bashtic function it's first action is to
restore the configuration of the current location and backend(s) in case these
variables were overwritten by your pipeline.

For example even if your pipeline accidentally clobbered the forget policy:

```bash
forget=() # whoops, but actually it's safe
bashtic_forget
```

The `bashtic_forget` function will retrieve the real forget variable from the
current location and use it instead. This means you don't need to worry about
how you name your variables and bashtic does not need to put any restrictions on
your pipelines.

### Error handling

The bashtic functions will exit if anything goes wrong inside them to prevent
accidents. Error handling in the rest of the pipeline is entirely up to you.

### Examples

Let's allow locations to use a `custom_file_copy` array variable to define files
that should be copied in the clear to the same location as our repo.

Imagine a location configuration:

```bash
from=("$HOME")
to=("localdir")
custom_file_copy=("$HOME/README.md")
```

We omit the backend configuration as it's trivial here.

We can now write a pipeline to copy the files in `custom_file_copy` before the
backup:

```bash
copied="false"
if [[ "${#custom_file_copy[@]}" -le 0 ]]; then
  echo "No files were specified for copying!"
fi
for fc in "${custom_file_copy[@]}"
do
  cp --preserve "$fc" "$path/../" || exit 1
  copied="true"
done
if [[ "$copied" != "true" ]]; then
  echo "We failed to copy anything!" ; exit 1
fi

bashtic_backup
bashtic_check
bashtic_forget
bashtic_check
```

As a bonus we abort if no files were specified and we run a backup, check, forget
and check again. Note the use of `path` to find the restic repository, this was
set by the backend configuration.
