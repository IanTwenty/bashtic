#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# A BASH wrapper for restic
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

bats_require_minimum_version 1.5.0

source "$BATS_TEST_DIRNAME"/../../bashtic
load ../test_helper/bats-support/load
load ../test_helper/bats-assert/load

################################################################################
#
# Tests
#
################################################################################


@test "default pipelines are called" {
  backup_called="false"
  check_called="false"
  forget_called="false"
  bashtic_backup() {
    backup_called="true"
  }
  bashtic_check() {
    check_called="true"
  }
  bashtic_forget() {
    forget_called="true"
  }

  pipeline_run "backup" "/tmp" "/tmp"
  assert "$backup_called"

  pipeline_run "check" "/tmp" "/tmp"
  assert "$check_called"

  pipeline_run "forget" "/tmp" "/tmp"
  assert "$forget_called"
}

@test "file-based pipelines can override" {
  user_tmpdir=$(mktemp -d)
  echo "echo user" > "$user_tmpdir/backup"
  system_tmpdir=$(mktemp -d)
  echo "echo system" > "$system_tmpdir/backup"

  # NOTE: Because we want to override this function we can't use bats `run` below
  bashtic_backup() {
    echo "default"
  }

  # User pipeline runs, even though system one present
  output=$(pipeline_run "backup" "$user_tmpdir" "$system_tmpdir")
  assert [ "$output" == "user" ]

  # System pipeline runs
  rm "$user_tmpdir/backup"
  output=$(pipeline_run "backup" "$user_tmpdir" "$system_tmpdir")
  assert [ "$output" == "system" ]

  # Finally default pipeline runs. Note we don't use bats 'run'
  rm "$system_tmpdir/backup"
  output=$(pipeline_run "backup" "$user_tmpdir" "$system_tmpdir")
  assert [ "$output" == "default" ]
}

@test "file-based pipeline ok" {
  tmpdir=$(mktemp -d)
  cat > "$tmpdir/mypipeline" << EOF
touch $tmpdir/i_ran
EOF

  pipeline_run "mypipeline" "$tmpdir" "/tmp"
  assert [ -e "$tmpdir/i_ran" ]
}

@test "if default pipeline fails we fail" {
  bashtic_backup() {
    exit 99
  }

  run -99 pipeline_run "backup" "/tmp"
}

@test "file-based pipeline fails we fail" {
  tmpdir=$(mktemp -d)
  cat > "$tmpdir/mypipeline" << EOF
exit 99
EOF

  run -99 pipeline_run "mypipeline" "$tmpdir"
}

@test "missing file-based pipeline fails" {
  run ! pipeline_run "nonexistant" "/tmp" "/tmp/system"
  assert_output "No pipeline found. Tried /tmp/nonexistant and /tmp/system/nonexistant"
}
