#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# A BASH wrapper for restic
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

bats_require_minimum_version 1.5.0

source "$BATS_TEST_DIRNAME"/../../bashtic
load ../test_helper/bats-support/load
load ../test_helper/bats-assert/load
load ../test_helper/bats-mock/load

restic() {
  $restic_cmd "$@"
}

setup_file() {
  restic_cmd="$(mock_create)"
  export restic_cmd
  export -f restic
}

################################################################################
#
# Tests
#
################################################################################

@test "if restore fails we fail" {
  mock_set_status "$restic_cmd" 1
  run ! bashtic_restore
}

@test "restore uses all the basic vars we pass" {
  tmpfile=$(mktemp)
  mock_set_status "$restic_cmd" 0
  mock_set_side_effect "$restic_cmd" "echo "\$@" > $tmpfile"
  local path="repo"
  local bashtic_restoreto="/dir"
  local bashtic_snapshot="latest"
  local restore_flags=("--include" "/something")

  bashtic_restore
  assert_equal "$(cat "$tmpfile")" "restore -r repo latest --target /dir --include /something"
}

@test "restore fails if required vars not set" {
  tmpfile=$(mktemp)
  mock_set_status "$restic_cmd" 0
  mock_set_side_effect "$restic_cmd" "echo "\$@" > $tmpfile"
  local path="repo"
  local bashtic_restoreto="/dir"

  run bashtic_restore
  assert_failure ; assert_output "ERROR: Cannot restore - no snapshot provided on cmdline."

  unset bashtic_restoreto
  local bashtic_snapshot="latest"

  run bashtic_restore
  assert_failure ; assert_output "ERROR: Cannot restore - no restore dir provided on cmdline."
}
