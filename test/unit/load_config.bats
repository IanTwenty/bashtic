#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# A BASH wrapper for restic
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/bashtic/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

bats_require_minimum_version 1.5.0

source "$BATS_TEST_DIRNAME"/../../bashtic
load ../test_helper/bats-support/load
load ../test_helper/bats-assert/load

################################################################################
#
# NOTE!
#
################################################################################

# We do not use 'run' for most of these tests as they are run in a subshell
# and thus we can't see if load_config affected our environment.

################################################################################
#
# Tests
#
################################################################################

@test "config must exist" {
  run ! load_config "nonexistant"
  assert_output "attempt to load non-existant config nonexistant"
}

@test "config with an error fails" {
  tmpconfig=$(mktemp)
  echo "exit 99" > "$tmpconfig"

  run -99 load_config "$tmpconfig"
}

@test "config cannot interfere with our environment unless matching vars are selected" {
  tmpconfig=$(mktemp)
  blah=safe
  echo "blah=notsafe" > "$tmpconfig"

  # bats must set -e during tests to detect failure but we want to run our
  # function in this environment and it has a grep that's allowed to fail
  set +e
  load_config "$tmpconfig"
  set -e

  assert_equal "$blah" "safe"
}

@test "config cannot interfere with our environment when wrong vars are selected" {
  tmpconfig=$(mktemp)
  blah=safe
  echo "blah=notsafe" > "$tmpconfig"

  # bats must set -e during tests to detect failure but we want to run our
  # function in this environment and it has a grep that's allowed to fail
  set +e
  load_config "$tmpconfig" "notblah"
  set -e

  assert_equal "$blah" "safe"
}

@test "config can set one allowed variable in our environment" {
  tmpconfig=$(mktemp)
  stuff=changeme
  vars=("stuff")
  echo "stuff=changed" > "$tmpconfig"

  load_config "$tmpconfig" "${vars[@]}"

  assert_equal "$stuff" "changed"
}

@test "config can set many allowed variables in our environment" {
  tmpconfig=$(mktemp)
  stuff=changeme
  morestuff=changeme
  vars=("stuff" "morestuff")
  echo $'stuff=changed\nmorestuff=changed' > "$tmpconfig"

  load_config "$tmpconfig" "${vars[@]}"

  assert_equal "$stuff" "changed"
  assert_equal "$morestuff" "changed"
}

@test "config can set array variables in our environment" {
  tmpconfig=$(mktemp)
  stuff=("change" "me")
  vars=("stuff")
  echo $'stuff=("changed" "array")' > "$tmpconfig"

  load_config "$tmpconfig" "${vars[@]}"

  assert_equal "${stuff[*]}" "changed array"
}

@test "config can set associative array variables in our environment" {
  tmpconfig=$(mktemp)
  stuff=(["1"]="one")
  vars=("stuff")
  echo $'stuff=(["1"]="changed" ["2"]="this")' > "$tmpconfig"

  load_config "$tmpconfig" "${vars[@]}"

  assert_equal "${!stuff[*]}" "1 2"
  assert_equal "${stuff[*]}" "changed this"
}

@test "all vars prefixed custom_ are set in our environment" {
  tmpconfig=$(mktemp)
  echo $'custom_var=me\ncustom_var2=two' > "$tmpconfig"

  load_config "$tmpconfig"

  # shellcheck disable=SC2154
  assert_equal "$custom_var" "me"
  # shellcheck disable=SC2154
  assert_equal "$custom_var2" "two"
}
